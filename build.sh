#!/busybox/sh

if [ "$#" -ne 2 ]; then
  echo "usage: $0 <context-directory> <container-name>"
  exit 1
fi

mkdir -p "/kaniko/.docker"

cat > "/kaniko/.docker/config.json" <<EOF
{
  "auths": {
    "${CI_REGISTRY}": {
      "username": "${CI_REGISTRY_USER}",
      "password": "${CI_REGISTRY_PASSWORD}"
    }
  }
}
EOF

CONTEXT_DIR="$1"
CONTAINER_NAME="$2"

/kaniko/executor --context "${CONTEXT_DIR}" \
                 --dockerfile "${CONTEXT_DIR}/Dockerfile" \
                 --destination ${CI_REGISTRY_IMAGE}/${CONTAINER_NAME}:latest
